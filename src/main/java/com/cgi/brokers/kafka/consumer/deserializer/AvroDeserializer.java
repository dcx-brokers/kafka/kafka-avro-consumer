package com.cgi.brokers.kafka.consumer.deserializer;

import java.util.Arrays;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificRecordBase;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AvroDeserializer<T extends SpecificRecordBase> implements Deserializer<T>
{
    /**
     * Logger de la classe.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AvroDeserializer.class);

    private final Class<T> targetType;

    public AvroDeserializer(final Class<T> targetType)
    {
        this.targetType = targetType;
    }

    @Override
    public void configure(final Map<String, ?> theMap, final boolean theB)
    {

    }

    @SuppressWarnings("unchecked")
    @Override
    public T deserialize(final String topic, final byte[] data)
    {
        try
        {
            T result = null;

            if (data != null)
            {
                LOGGER.debug("data='{}'", DatatypeConverter.printHexBinary(data));

                final DatumReader<GenericRecord> datumReader = new SpecificDatumReader<>(this.targetType.newInstance().getSchema());
                final Decoder decoder = DecoderFactory.get().binaryDecoder(data, null);

                result = (T) datumReader.read(null, decoder);
                LOGGER.debug("deserialized data='{}'", result);
            }
            return result;
        } catch (final Exception ex)
        {
            throw new SerializationException(
                    "Can't deserialize data '" + Arrays.toString(data) + "' from topic '" + topic + "'", ex);
        }
    }

    @Override
    public void close()
    {

    }
}
