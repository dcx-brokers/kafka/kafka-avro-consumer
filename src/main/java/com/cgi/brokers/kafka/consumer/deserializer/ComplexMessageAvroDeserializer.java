package com.cgi.brokers.kafka.consumer.deserializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cgi.brokers.kafka.model.ComplexMessage;

public class ComplexMessageAvroDeserializer extends AvroDeserializer<ComplexMessage>
{
    /**
     * Logger de la classe.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ComplexMessageAvroDeserializer.class);

    public ComplexMessageAvroDeserializer()
    {
        super(ComplexMessage.class);
    }
}
