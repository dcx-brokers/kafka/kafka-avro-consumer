package com.cgi.brokers.kafka.consumer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cgi.brokers.kafka.consumer.deserializer.ComplexMessageAvroDeserializer;
import com.cgi.brokers.kafka.consumer.properties.PropertiesLoader;
import com.cgi.brokers.kafka.model.ComplexMessage;

/**
 * Consommateur de messages pour Kafka.
 *
 * @author damien.cacheux
 */
public class CustomKafkaConsumer
{
    /**
     * Logger de la classe.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomKafkaConsumer.class);

    private static Properties props;

    /**
     * Méthode principale de la classe
     *
     * @param args Paramètres de la méthode (args[0] : indique si l'on souhaite afficher les messages)
     * @throws Exception Exception relevée lors de l'exécution
     */
    public static void main(final String[] args) throws Exception
    {
        CustomKafkaConsumer.props = new PropertiesLoader()
                .withFiles("consumer.properties")
                .get();

        if (args.length == 0)
        {
            CustomKafkaConsumer.runConsumer(true);
        } else
        {
            CustomKafkaConsumer.runConsumer(Boolean.parseBoolean(args[0]));
        }
    }

    /**
     * Exécute le producteur et envoie un nombre de messages spécifiques.
     */
    static void runConsumer(final boolean afficheMessages) throws InterruptedException
    {
        final Consumer<Long, ComplexMessage> consumer = CustomKafkaConsumer.createConsumer();

        final int giveUp = 100;
        int noRecordsCount = 0;
        final int[] nbReceivedMessage = {0};

        while (true)
        {
            final ConsumerRecords<Long, ComplexMessage> consumerRecords = consumer.poll(Duration.ofMinutes(1L));

            if (consumerRecords.count() == 0)
            {
                noRecordsCount++;
                if (noRecordsCount > giveUp)
                {
                    break;
                } else
                {
                    continue;
                }
            }

            consumerRecords.forEach(record -> {
                if (afficheMessages)
                {
                    LOGGER.info(String.format("Consumer record : (%d, %s, %d, %s)\n", record.key(), String.format("%s - %s", record.value().getMessage(), record.value().getCurrentDate()), record.partition(), record.offset()));
                }
                nbReceivedMessage[0]++;
            });

            if (nbReceivedMessage[0] % 100 == 0)
            {
                LOGGER.info(String.format("Nombre de messages reçus : %s", nbReceivedMessage[0]));
            }
            consumer.commitAsync();
        }

        consumer.close();
        System.out.println("Done");
    }

    /**
     * Initialise le producteur de messages pour Kafka.
     *
     * @return Le producteur initialisé
     */
    private static Consumer<Long, ComplexMessage> createConsumer()
    {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CustomKafkaConsumer.props.getProperty("bootstrap.servers"));
        props.put(ConsumerConfig.GROUP_ID_CONFIG, CustomKafkaConsumer.props.getProperty("consumer.group.id"));
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ComplexMessageAvroDeserializer.class);

        final Consumer<Long, ComplexMessage> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singleton(CustomKafkaConsumer.props.getProperty("consumer.topic")));
        return consumer;
    }
}
