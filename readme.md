# Consommateur de messages Kafka
Le but du projet est de consommer des messages _complexes_, depuis un serveur ou un cluster Apache Kafka / Confluent Kafka

## Préparation
Il faut copier le fichier `config/consumer.properties`, et adapter les valeurs.  
  * `topic` est le topic dans lequel le consumer doit lire.  
  * `bootstrap.servers` indique le ou les serveurs qui forment le cluster Kafka.
  * `consumer.group.id` indique le gorupe auquel appartient le consumer.

## Lancement
Le fichier `doc/kafka-consumer.jar` doit être copié dans le même répertoire que le fichier `consumer.properties`.  
La commande à lancer est
  * `java -cp kafka-consumer.jar com.cgi.brokers.kafka.consumer.CustomKafkaConsumer [AFFICHE_MESSAGES]`  

Le paramètre `AFFICHE_MESSAGES` est facultatif. S'il n'est pas présent, la valeur vaut `true`, sinon la velaur passée en paramètre. Ce paramètre indique si l'on affiche les messages reçus.  

## Détails
Durant l'exécution du programme, il va afficher chaque message qu'il a reçu. Il ne faut pas forcément tenir en compte les temps d'exécution car les affichages consoles prennent énormément de temps.

## Objet lu lors de la consommation
Afin de présenter la consommation avec un objet _complexe_, le format de l'objet utilisé est le suivant :
  * `Identifiant` : l'identifiant est sous forme d'UUID
  * `Message` : le message émis sous forme de String
  * `currentDate` : la date de création du message

